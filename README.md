# Technical Test Part 2

## Interaction

The interaction flow starts from the `AAM_PlayerController`, when it receives an input from the player, it calls the `InteractWithCurrentInteractable()` method in `UAM_PlayerInteractorComponent` component found in the `PossessedPawn` of the controller.

The `UAM_PlayerInteractorComponent` has a reference to a nearby actor that has the interface `UAM_InteractableInterface` and is using `Interactable` collision preset. The `CurrentInteractable` pointer will be invalid if there is no nearby interactable.

When interaction is triggered, the method `OnInteracted()` is called. In the example of `AAM_ElevatorCaller`, when interacted, it finds the `UAM_ElevatorSubsystem` which inherits from a `UWorldSubsystem`, and call its `CallElevator()` method, so that it has a reference to the information of the current player. The subsystem then tell `AAM_PlayerController` to show `UAM_ElevatorCallerHUD` widget in the HUD.

## Subsystem

There is a `UAM_ElevatorSubsystem` that is responsible as a middleman between player and the elevators in the world. I didn't want the elevator caller to has a reference to player related classes, as I find it to be unneccessary. I also make sure that the `UAM_ElevatorSubsystem` will never exist on a server.

## Elevator System

Once player selected a direction from a specific floor, the player controller will make a ServerRPC call to the elevator existing in the server. The elevator will then add the request to a `TMap` stored within the elevator actor itself.

In every tick, the elevator checks for existing request, and find the closest floor that also align with its current movement direction. When it has acquired a target, it will start moving towards it by linearly intepolate to that position.

The position of the elevator is replicated to every client instances.

## Notes

Currently, the elevator system is not working as intended, I didn't have the time to debug it. I understand that this result only meets one of the requirement, as I have spent most of my time figuring out an extendable solution to the elevator and interaction systems. There should be a better way to calculate an optimal target for the elevator, but I wasn't able to redesign it.

I have took server authority into consideration when designing this project, but currently there is an issue with the interactable when the game is running in Client netmode.

The project is using the old input system because that's the default for Unreal Engine 5.0, but I know how to use the EnhancedInput system.
