// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class AmmoboxTechnicalTest : ModuleRules
{
	public AmmoboxTechnicalTest(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "UMG" });
	}
}
