// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable/AM_InteractableInterface.h"
#include "GameFramework/Actor.h"
#include "AM_ElevatorActor.h"
#include "Enums/ElevatorEnum.h"
#include "AM_ElevatorCaller.generated.h"

UCLASS()
class AMMOBOXTECHNICALTEST_API AAM_ElevatorCaller : public AActor, public IAM_InteractableInterface
{
	GENERATED_BODY()
	
public:
	AAM_ElevatorCaller();

public:
    virtual void OnInteracted() override;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UStaticMeshComponent> ElevaterCallerStaticMesh;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "References", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<AAM_ElevatorActor> ConnectedElevator;

    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings", meta = (AllowPrivateAccess = "true"))
    int CallerFloorId;
};
