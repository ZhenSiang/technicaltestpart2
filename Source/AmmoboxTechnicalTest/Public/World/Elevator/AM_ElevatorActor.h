// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h" 
#include "Enums/ElevatorEnum.h"
#include "Net/UnrealNetwork.h"
#include "AM_ElevatorActor.generated.h"

UCLASS()
class AMMOBOXTECHNICALTEST_API AAM_ElevatorActor : public AActor
{
	GENERATED_BODY()
	
public:
	AAM_ElevatorActor();
	virtual void Tick(float DeltaTime) override;

	void CallElevator(int InFloorId, EAM_ElevatorDirection ElevatorDirection);

protected:	
	virtual void BeginPlay() override;

private:
	void ServerTickElevatorMovement(const float DeltaTime);
	void ServerTickElevatorWait(const float DeltaTime);
	void ServerProceedToTargetFloor();
	void CalculateTargetFloorId();

	FORCEINLINE bool IsElevatorWaiting() { return PreviousTargetFloorId == CurrentTargetFloorId; }

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor *OverlappedActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor *OverlappedActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex);

	FORCEINLINE float GetTargetHeightFromFloorId(int InFloorId) { return InFloorId * 300.f; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UStaticMeshComponent> ElevatorStaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	TObjectPtr<UShapeComponent> ElevatorTrigger;

	FORCEINLINE int32 GetActorsInElevatorCount() { return ActorsInElevatorSet.Num(); }

	TMap<int32, EAM_ElevatorDirection> CurrentElevatorRequests;
	EAM_ElevatorDirection CurrentElevatorDirection;
	TSet<AActor*> ActorsInElevatorSet;

	float ElevatorMoveLerpValue = 0.f;
	float ElevatorWaitTimeCounter = 0.f;

	int MaxFloorId = 4;

	UPROPERTY(VisibleAnywhere, Category = "Runtime", meta = (AllowPrivateAccess = "true"))
	int CurrentTargetFloorId;

	int CurrentNextFloorId;
	int PreviousTargetFloorId;
};
