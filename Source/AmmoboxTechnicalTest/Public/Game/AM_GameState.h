// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "Game/AM_ElevatorGameStateComponent.h"
#include "AM_GameState.generated.h"

UCLASS()
class AMMOBOXTECHNICALTEST_API AAM_GameState : public AGameStateBase
{
	GENERATED_BODY()

    AAM_GameState();

private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    TObjectPtr<UAM_ElevatorGameStateComponent> ElevatorGameStateComponent;
};