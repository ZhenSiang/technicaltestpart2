// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h" 
#include "Interactable/AM_InteractableInterface.h"
#include "AM_PlayerInteractorComponent.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class AMMOBOXTECHNICALTEST_API UAM_PlayerInteractorComponent : public USphereComponent
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;

    bool InteractWithCurrentInteractable();

private:
    UFUNCTION()
    void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor *OverlappedActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

    UFUNCTION()
    void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor *OverlappedActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex);

    UPROPERTY()
    TObjectPtr<UObject> CurrentInteractable;
};
