// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Player/AM_PlayerInteractorComponent.h"
#include "Net/UnrealNetwork.h"
#include "World/Elevator/AM_ElevatorActor.h"
#include "Enums/ElevatorEnum.h"
#include "AM_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class AMMOBOXTECHNICALTEST_API AAM_PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	void StartInteraction();
	void OnStartChoosingDirection();

	UFUNCTION(Server, Reliable)
	void ServerSelectFloor(AAM_ElevatorActor* Elevator, int FloorId, EAM_ElevatorDirection ElevatorDirection);

	void SelectFloor(AAM_ElevatorActor* Elevator, int FloorId, EAM_ElevatorDirection ElevatorDirection);
	void OnFloorSelected();

protected:
	virtual void SetupInputComponent() override;
	virtual void OnPossess(APawn* PossessedPawn) override;

private:
	TObjectPtr<UAM_PlayerInteractorComponent> PlayerInteractorComponent;
	bool bIsInElevator = false;
};
