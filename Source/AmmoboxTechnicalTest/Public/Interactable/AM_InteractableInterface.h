// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AM_InteractableInterface.generated.h"

UINTERFACE(MinimalAPI)
class UAM_InteractableInterface : public UInterface
{
    GENERATED_BODY()
};

class IAM_InteractableInterface
{
    GENERATED_BODY()

public:
    virtual void OnInteracted() {}
};