// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Enums/ElevatorEnum.h"
#include "Subsystems/AM_ElevatorSubsystem.h"
#include "AM_ElevatorCallerHUD.generated.h"

/**
 * 
 */
UCLASS()
class AMMOBOXTECHNICALTEST_API UAM_ElevatorCallerHUD : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void CallElevatorInDirection(EAM_ElevatorDirection ElevatorDirection)
	{
		GetWorld()->GetSubsystem<UAM_ElevatorSubsystem>()->OnElevatorCalled(ElevatorDirection);
	}
};