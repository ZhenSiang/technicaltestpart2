// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "AM_ElevatorCallerHUD.h"
#include "AM_HUD.generated.h"

/**
 * 
 */
UCLASS()
class AMMOBOXTECHNICALTEST_API AAM_HUD : public AHUD
{
	GENERATED_BODY()

	virtual void BeginPlay() override;

public:
	void ShowElevatorCallerWidget();
	void HideElevatorCallerWidget();

private:
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UAM_ElevatorCallerHUD> ElevatorHUDClass;

	UPROPERTY(VisibleInstanceOnly)
	TObjectPtr<UAM_ElevatorCallerHUD> ElevatorCallerHUD;
};
