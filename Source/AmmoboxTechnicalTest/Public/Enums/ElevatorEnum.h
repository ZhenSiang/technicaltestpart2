// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EAM_ElevatorDirection : uint8
{
    None,
    Up,
    Down,
    Both
};