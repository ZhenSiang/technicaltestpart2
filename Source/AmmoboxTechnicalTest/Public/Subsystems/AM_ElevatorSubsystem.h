// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Enums/ElevatorEnum.h"
#include "World/Elevator/AM_ElevatorActor.h"
#include "Player/AM_PlayerController.h"
#include "AM_ElevatorSubsystem.generated.h"

UCLASS()
class AMMOBOXTECHNICALTEST_API UAM_ElevatorSubsystem : public UWorldSubsystem
{
    GENERATED_BODY()

    virtual bool ShouldCreateSubsystem(UObject* Outer) const override;
    virtual void Initialize(FSubsystemCollectionBase& Collection) override;

public:
    void CallElevator(int FloorId, AAM_ElevatorActor* ElevatorToCall);
    void OnElevatorCalled(EAM_ElevatorDirection ElevatorDirection);

private:
    void CacheReferences();

    TObjectPtr<class AAM_HUD> HUD;
    TObjectPtr<AAM_PlayerController> PlayerController;
    int CurrentInteractingFloorId;
    TWeakObjectPtr<AAM_ElevatorActor> CurrentInteractingElevator;
};
