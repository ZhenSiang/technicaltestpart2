// Copyright Epic Games, Inc. All Rights Reserved.

#include "AmmoboxTechnicalTestGameMode.h"
#include "AmmoboxTechnicalTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAmmoboxTechnicalTestGameMode::AAmmoboxTechnicalTestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
