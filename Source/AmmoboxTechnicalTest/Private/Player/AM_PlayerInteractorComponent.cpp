#include "Player/AM_PlayerInteractorComponent.h"

void UAM_PlayerInteractorComponent::BeginPlay()
{
    Super::BeginPlay();

    OnComponentBeginOverlap.AddDynamic(this, &UAM_PlayerInteractorComponent::OnBeginOverlap);
    OnComponentEndOverlap.AddDynamic(this, &UAM_PlayerInteractorComponent::OnEndOverlap);
}

bool UAM_PlayerInteractorComponent::InteractWithCurrentInteractable()
{
    if (!IsValid(CurrentInteractable))
        return false;

    Cast<IAM_InteractableInterface>(CurrentInteractable)->OnInteracted();
    return true;
}

void UAM_PlayerInteractorComponent::OnBeginOverlap(UPrimitiveComponent *OverlappedComponent, AActor *OverlappedActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
    if (OverlappedActor->Implements<UAM_InteractableInterface>())
    {
        CurrentInteractable = OverlappedActor;
        UE_LOG(LogTemp, Log, TEXT("UAM_PlayerInteractorComponent: Within interactable range named %s"), *OverlappedActor->GetName())
    }
}

void UAM_PlayerInteractorComponent::OnEndOverlap(UPrimitiveComponent *OverlappedComponent, AActor *OverlappedActor, UPrimitiveComponent *OtherComp, int32 OtherBodyIndex)
{
    if (OverlappedActor->Implements<UAM_InteractableInterface>())
    {
        CurrentInteractable = nullptr;
        UE_LOG(LogTemp, Log, TEXT("UAM_PlayerInteractorComponent: Not within interactable range anymore"))
    }
}