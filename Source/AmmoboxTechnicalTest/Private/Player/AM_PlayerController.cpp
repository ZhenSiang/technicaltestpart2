// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/AM_PlayerController.h"

void AAM_PlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();

    InputComponent->BindAction("Interact", IE_Pressed, this, &AAM_PlayerController::StartInteraction);
}

void AAM_PlayerController::OnPossess(APawn* PossessedPawn)
{
    Super::OnPossess(PossessedPawn);

    PlayerInteractorComponent = PossessedPawn->FindComponentByClass<UAM_PlayerInteractorComponent>();
}

void AAM_PlayerController::StartInteraction()
{
    bool bSuccess = PlayerInteractorComponent->InteractWithCurrentInteractable();
}

void AAM_PlayerController::OnStartChoosingDirection()
{
    SetInputMode(FInputModeUIOnly());
    SetShowMouseCursor(true);
}

void AAM_PlayerController::SelectFloor(AAM_ElevatorActor* Elevator, int FloorId, EAM_ElevatorDirection ElevatorDirection)
{
    OnFloorSelected();
    ServerSelectFloor(Elevator, FloorId, ElevatorDirection);
}

void AAM_PlayerController::ServerSelectFloor_Implementation(AAM_ElevatorActor* Elevator, int FloorId, EAM_ElevatorDirection ElevatorDirection)
{
    Elevator->CallElevator(FloorId, ElevatorDirection);
}

void AAM_PlayerController::OnFloorSelected()
{
    SetInputMode(FInputModeGameOnly());
    SetShowMouseCursor(false);
}
