#include "Game/AM_ElevatorGameStateComponent.h"
#include "Game/AM_GameState.h"

AAM_GameState::AAM_GameState()
{
    ElevatorGameStateComponent = CreateDefaultSubobject<UAM_ElevatorGameStateComponent>(TEXT("Elevator Component"));
}