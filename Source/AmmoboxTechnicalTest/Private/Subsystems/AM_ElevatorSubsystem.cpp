#include "Subsystems/AM_ElevatorSubsystem.h"
#include "HUD/AM_HUD.h"

bool UAM_ElevatorSubsystem::ShouldCreateSubsystem(UObject* Outer) const
{
#if UE_SERVER
    return false;
#else
    return true;
#endif
}

void UAM_ElevatorSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
    Super::Initialize(Collection);

}

void UAM_ElevatorSubsystem::CacheReferences()
{
    if (!IsValid(HUD))
    {
        UWorld* WorldContext = GetWorld();
        PlayerController = Cast<AAM_PlayerController>(GEngine->GetGamePlayer(WorldContext, 0)->GetPlayerController(WorldContext));
        HUD = Cast<AAM_HUD>(PlayerController->GetHUD());
    }
}

void UAM_ElevatorSubsystem::CallElevator(int FloorId, AAM_ElevatorActor* ElevatorToCall)
{
    CacheReferences();

    CurrentInteractingFloorId = FloorId;
    CurrentInteractingElevator = ElevatorToCall;
    HUD->ShowElevatorCallerWidget();

    PlayerController->OnStartChoosingDirection();
}

void UAM_ElevatorSubsystem::OnElevatorCalled(EAM_ElevatorDirection ElevatorDirection)
{
    CacheReferences();

    HUD->HideElevatorCallerWidget();

    if (!CurrentInteractingElevator.IsValid())
    {
        UE_LOG(LogTemp, Error, TEXT("UAM_ElevatorSubsystem: Called elevator but currently there is no elevator assigned"))
        return;
    }

    PlayerController->SelectFloor(CurrentInteractingElevator.Get(), CurrentInteractingFloorId, ElevatorDirection);
}
