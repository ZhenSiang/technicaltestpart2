// Fill out your copyright notice in the Description page of Project Settings.


#include "World/Elevator/AM_ElevatorActor.h"

AAM_ElevatorActor::AAM_ElevatorActor()
{
	bReplicates = true;
	SetReplicateMovement(true);

	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = SceneComponent;

	ElevatorStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Elevator Mesh"));
	ElevatorStaticMesh->SetupAttachment(RootComponent);

	ElevatorTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Elevator Trigger"));
	ElevatorTrigger->SetupAttachment(RootComponent);

	RootComponent->SetRelativeScale3D(FVector(3.f, 3.f, 1.f));
}

// Called when the game starts or when spawned
void AAM_ElevatorActor::BeginPlay()
{
	Super::BeginPlay();

	ElevatorTrigger->OnComponentBeginOverlap.AddDynamic(this, &AAM_ElevatorActor::OnBeginOverlap);
	ElevatorTrigger->OnComponentEndOverlap.AddDynamic(this, &AAM_ElevatorActor::OnEndOverlap);
	
}

// Called every frame
void AAM_ElevatorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!HasAuthority())
		return;

	ServerTickElevatorMovement(DeltaTime);
	ServerTickElevatorWait(DeltaTime);
	CalculateTargetFloorId();
}

void AAM_ElevatorActor::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OverlappedActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	ActorsInElevatorSet.Add(OverlappedActor);

	UE_LOG(LogTemp, Log, TEXT("AAM_ElevatorActor: Overlapped with %s, count: %d"), *OverlappedActor->GetName(), GetActorsInElevatorCount());
}

void AAM_ElevatorActor::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OverlappedActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ActorsInElevatorSet.Remove(OverlappedActor);

	UE_LOG(LogTemp, Log, TEXT("AAM_ElevatorActor: Stop overlapping with %s, count: %d"), *OverlappedActor->GetName(), GetActorsInElevatorCount());
}

void AAM_ElevatorActor::ServerTickElevatorMovement(const float DeltaTime)
{
	if (IsElevatorWaiting())
		return;

	ElevatorMoveLerpValue += DeltaTime;

	float PreviousHeight = GetTargetHeightFromFloorId(PreviousTargetFloorId);
	float NextHeight = GetTargetHeightFromFloorId(CurrentNextFloorId);

	float Height = FMath::Lerp(PreviousHeight, NextHeight, ElevatorMoveLerpValue);

	FVector CurrentLocation = GetActorLocation();
	SetActorLocation(FVector(CurrentLocation.X, CurrentLocation.Y, Height));

	if (ElevatorMoveLerpValue >= 1.f)
	{
		PreviousTargetFloorId = CurrentNextFloorId;

		if (CurrentNextFloorId == CurrentTargetFloorId)
		{
			if (!CurrentElevatorRequests.Contains(CurrentNextFloorId))
			{
				UE_LOG(LogTemp, Error, TEXT("CurrentElevatorRequests does not contain %d but it is the target"), CurrentTargetFloorId)
				return;
			}

			ElevatorWaitTimeCounter = 0.f;

			bool IsUp = CurrentElevatorDirection == EAM_ElevatorDirection::Up;
			if (IsUp)
			{
				if (CurrentElevatorRequests[CurrentTargetFloorId] == EAM_ElevatorDirection::Up)
				{
					CurrentElevatorRequests.Remove(CurrentTargetFloorId);
				}
				else if (CurrentElevatorRequests[CurrentTargetFloorId] == EAM_ElevatorDirection::Both)
				{
					CurrentElevatorRequests.Add(CurrentTargetFloorId, EAM_ElevatorDirection::Down);
				}
			}
			else
			{
				if (CurrentElevatorRequests[CurrentTargetFloorId] == EAM_ElevatorDirection::Down)
				{
					CurrentElevatorRequests.Remove(CurrentTargetFloorId);
				}
				else if (CurrentElevatorRequests[CurrentTargetFloorId] == EAM_ElevatorDirection::Both)
				{
					CurrentElevatorRequests.Add(CurrentTargetFloorId, EAM_ElevatorDirection::Down);
				}
			}
		}
	}
}

void AAM_ElevatorActor::ServerTickElevatorWait(const float DeltaTime)
{
	if (!IsElevatorWaiting())
		return;

	ElevatorWaitTimeCounter += DeltaTime;

	if (ElevatorWaitTimeCounter > 3.f)
	{
		ServerProceedToTargetFloor();
	}
}

void AAM_ElevatorActor::CalculateTargetFloorId()
{
	if (CurrentElevatorRequests.IsEmpty())
		return;

	int FloorId = CurrentTargetFloorId;

	if (FloorId == 0)
	{
		CurrentElevatorDirection = EAM_ElevatorDirection::Up;
	}
	else if (FloorId == MaxFloorId)
	{
		CurrentElevatorDirection = EAM_ElevatorDirection::Down;
	}

	const bool bIsUp = CurrentElevatorDirection == EAM_ElevatorDirection::Up;
	bool bFoundTarget = false;

	while (FloorId >= 0 && FloorId <= MaxFloorId)
	{
		int NextFloodIdToCheck = FloorId + (bIsUp ? 1 : -1);
		if (CurrentElevatorRequests.Contains(NextFloodIdToCheck) && CurrentElevatorRequests[NextFloodIdToCheck] == (bIsUp ? EAM_ElevatorDirection::Up : EAM_ElevatorDirection::Down))
		{
			CurrentTargetFloorId = NextFloodIdToCheck;
			bFoundTarget = true;
			break;
		}

		FloorId += (bIsUp ? 1 : -1);
	}

	if (!bFoundTarget)
	{
		CurrentElevatorDirection = bIsUp ? EAM_ElevatorDirection::Down : EAM_ElevatorDirection::Up;
	}
}

void AAM_ElevatorActor::ServerProceedToTargetFloor()
{
	if (CurrentNextFloorId > CurrentTargetFloorId)
	{
		CurrentNextFloorId--;
	}
	else if (CurrentNextFloorId < CurrentTargetFloorId)
	{
		CurrentNextFloorId++;
	}
}

void AAM_ElevatorActor::CallElevator(int InFloorId, EAM_ElevatorDirection ElevatorDirection)
{
	if (!HasAuthority())
	{
		UE_LOG(LogTemp, Error, TEXT("AAM_ElevatorActor: Trying to call elevator on client side, not proceeding"));
		return;
	}

	//If the floor already has a request
	if (CurrentElevatorRequests.Contains(InFloorId))
	{
		if (CurrentElevatorRequests[InFloorId] != ElevatorDirection)
		{
			CurrentElevatorRequests.Add(InFloorId, EAM_ElevatorDirection::Both);
			return;
		}

		return;
	}

	CurrentElevatorRequests.Add(InFloorId, ElevatorDirection);
}
