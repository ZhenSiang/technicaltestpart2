// Fill out your copyright notice in the Description page of Project Settings.


#include "World/Elevator/AM_ElevatorCaller.h"
#include "Subsystems/AM_ElevatorSubsystem.h"

AAM_ElevatorCaller::AAM_ElevatorCaller()
{
    USceneComponent* SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = SceneComponent;

    ElevaterCallerStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Elevator Caller Mesh"));
	ElevaterCallerStaticMesh->SetupAttachment(RootComponent);
}

void AAM_ElevatorCaller::OnInteracted()
{
    UAM_ElevatorSubsystem* ElevatorSubsystem = GetWorld()->GetSubsystem<UAM_ElevatorSubsystem>();
    ElevatorSubsystem->CallElevator(CallerFloorId, ConnectedElevator);
}
