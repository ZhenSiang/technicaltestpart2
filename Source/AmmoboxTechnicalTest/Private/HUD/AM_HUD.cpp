// Fill out your copyright notice in the Description page of Project Settings.


#include "HUD/AM_HUD.h"
#include "Blueprint/UserWidget.h"

void AAM_HUD::BeginPlay()
{
    ElevatorCallerHUD = CreateWidget<UAM_ElevatorCallerHUD>(GetOwningPlayerController(), ElevatorHUDClass);
}

void AAM_HUD::ShowElevatorCallerWidget()
{
    ElevatorCallerHUD->AddToViewport();
}

void AAM_HUD::HideElevatorCallerWidget()
{
    ElevatorCallerHUD->RemoveFromViewport();
}
