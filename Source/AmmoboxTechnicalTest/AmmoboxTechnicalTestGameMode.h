// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AmmoboxTechnicalTestGameMode.generated.h"

UCLASS(minimalapi)
class AAmmoboxTechnicalTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAmmoboxTechnicalTestGameMode();
};



